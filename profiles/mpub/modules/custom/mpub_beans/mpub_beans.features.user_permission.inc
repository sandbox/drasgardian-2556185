<?php
/**
 * @file
 * mpub_beans.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function mpub_beans_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access bean overview'.
  $permissions['access bean overview'] = array(
    'name' => 'access bean overview',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'administer bean settings'.
  $permissions['administer bean settings'] = array(
    'name' => 'administer bean settings',
    'roles' => array(),
    'module' => 'bean',
  );

  // Exported permission: 'administer bean types'.
  $permissions['administer bean types'] = array(
    'name' => 'administer bean types',
    'roles' => array(),
    'module' => 'bean',
  );

  // Exported permission: 'administer beans'.
  $permissions['administer beans'] = array(
    'name' => 'administer beans',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'create any banner_image bean'.
  $permissions['create any banner_image bean'] = array(
    'name' => 'create any banner_image bean',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'create any featured_link bean'.
  $permissions['create any featured_link bean'] = array(
    'name' => 'create any featured_link bean',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'delete any banner_image bean'.
  $permissions['delete any banner_image bean'] = array(
    'name' => 'delete any banner_image bean',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'delete any featured_link bean'.
  $permissions['delete any featured_link bean'] = array(
    'name' => 'delete any featured_link bean',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'edit any banner_image bean'.
  $permissions['edit any banner_image bean'] = array(
    'name' => 'edit any banner_image bean',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'edit any featured_link bean'.
  $permissions['edit any featured_link bean'] = array(
    'name' => 'edit any featured_link bean',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'edit bean view mode'.
  $permissions['edit bean view mode'] = array(
    'name' => 'edit bean view mode',
    'roles' => array(),
    'module' => 'bean',
  );

  // Exported permission: 'view any banner_image bean'.
  $permissions['view any banner_image bean'] = array(
    'name' => 'view any banner_image bean',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'view any featured_link bean'.
  $permissions['view any featured_link bean'] = array(
    'name' => 'view any featured_link bean',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'content editor' => 'content editor',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'view bean page'.
  $permissions['view bean page'] = array(
    'name' => 'view bean page',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'view bean revisions'.
  $permissions['view bean revisions'] = array(
    'name' => 'view bean revisions',
    'roles' => array(),
    'module' => 'bean',
  );

  return $permissions;
}
