<?php
/**
 * @file
 * mpub_test_content.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mpub_test_content_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "openlayers" && $api == "openlayers_layers") {
    return array("version" => "1");
  }
}
