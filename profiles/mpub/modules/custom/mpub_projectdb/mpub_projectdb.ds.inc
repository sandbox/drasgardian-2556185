<?php
/**
 * @file
 * mpub_projectdb.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function mpub_projectdb_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|project|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'project';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'location_list' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'location_map' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|project|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function mpub_projectdb_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'location_list';
  $ds_field->label = 'Location List';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = 'project|*';
  $ds_field->properties = array(
    'block' => 'views|project_location_list-block_1',
    'block_render' => '1',
  );
  $export['location_list'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'location_map';
  $ds_field->label = 'Location Map';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = 'project|*';
  $ds_field->properties = array(
    'block' => 'views|project_location_map-block_1',
    'block_render' => '1',
  );
  $export['location_map'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function mpub_projectdb_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|project|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'project';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'title',
      ),
      'left' => array(
        1 => 'field_date',
        2 => 'field_project_status',
      ),
      'right' => array(
        3 => 'location_list',
        4 => 'location_map',
      ),
      'footer' => array(
        5 => 'body',
        6 => 'field_associated_agencies',
        7 => 'field_species_targetted',
        8 => 'field_officer',
        9 => 'field_oceanscape_goal',
        10 => 'field_notes',
        11 => 'field_existing_documents_links',
        12 => 'field_project_budget',
        13 => 'field_sdg_goal',
        14 => 'field_mdg_goal',
        15 => 'field_keywords',
      ),
    ),
    'fields' => array(
      'title' => 'header',
      'field_date' => 'left',
      'field_project_status' => 'left',
      'location_list' => 'right',
      'location_map' => 'right',
      'body' => 'footer',
      'field_associated_agencies' => 'footer',
      'field_species_targetted' => 'footer',
      'field_officer' => 'footer',
      'field_oceanscape_goal' => 'footer',
      'field_notes' => 'footer',
      'field_existing_documents_links' => 'footer',
      'field_project_budget' => 'footer',
      'field_sdg_goal' => 'footer',
      'field_mdg_goal' => 'footer',
      'field_keywords' => 'footer',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|project|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|project|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'project';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_2col_stacked_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'title',
        1 => 'field_project_status',
        2 => 'body',
        3 => 'group_project_dates',
        17 => 'field_date',
      ),
      'footer' => array(
        4 => 'group_location',
        16 => 'field_sdg_goal',
        18 => 'field_mdg_goal',
        20 => 'field_oceanscape_goal',
        21 => 'group_goals',
        31 => 'field_locations',
      ),
      'right' => array(
        5 => 'group_budget',
        6 => 'field_associated_agencies',
        7 => 'group_researchers',
        8 => 'group_agencies',
        9 => 'field_keywords',
        10 => 'field_species_targetted',
        11 => 'field_project_budget',
        12 => 'group_documents_links',
        13 => 'field_existing_documents_links',
        14 => 'group_keyword',
        15 => 'group_notes',
        19 => 'group_access_control',
        22 => 'field_notes',
        25 => 'field_access_control',
        26 => 'field_officer',
        28 => 'field_researcher',
      ),
      'hidden' => array(
        23 => 'path',
        24 => 'field_epog_id',
        27 => 'field_geo_study_loc_wkt',
        29 => 'field_funding_agency_id',
        30 => 'field_regions',
        32 => 'field_geo_study_loc',
        33 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'title' => 'left',
      'field_project_status' => 'left',
      'body' => 'left',
      'group_project_dates' => 'left',
      'group_location' => 'footer',
      'group_budget' => 'right',
      'field_associated_agencies' => 'right',
      'group_researchers' => 'right',
      'group_agencies' => 'right',
      'field_keywords' => 'right',
      'field_species_targetted' => 'right',
      'field_project_budget' => 'right',
      'group_documents_links' => 'right',
      'field_existing_documents_links' => 'right',
      'group_keyword' => 'right',
      'group_notes' => 'right',
      'field_sdg_goal' => 'footer',
      'field_date' => 'left',
      'field_mdg_goal' => 'footer',
      'group_access_control' => 'right',
      'field_oceanscape_goal' => 'footer',
      'group_goals' => 'footer',
      'field_notes' => 'right',
      'path' => 'hidden',
      'field_epog_id' => 'hidden',
      'field_access_control' => 'right',
      'field_officer' => 'right',
      'field_geo_study_loc_wkt' => 'hidden',
      'field_researcher' => 'right',
      'field_funding_agency_id' => 'hidden',
      'field_regions' => 'hidden',
      'field_locations' => 'footer',
      'field_geo_study_loc' => 'hidden',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|project|form'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function mpub_projectdb_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'countries';
  $ds_view_mode->label = 'Countries';
  $ds_view_mode->entities = array(
    'field_collection_item' => 'field_collection_item',
  );
  $export['countries'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'regions';
  $ds_view_mode->label = 'Regions';
  $ds_view_mode->entities = array(
    'field_collection_item' => 'field_collection_item',
  );
  $export['regions'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'titles_only';
  $ds_view_mode->label = 'Titles Only';
  $ds_view_mode->entities = array(
    'field_collection_item' => 'field_collection_item',
  );
  $export['titles_only'] = $ds_view_mode;

  return $export;
}
