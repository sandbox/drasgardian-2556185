Drupal.behaviors.openlayers_behavior_boxselections = {
  layer: null,
  control: null,
  last_box: null,
};


(function ($) {

  Drupal.openlayers.addBehavior('openlayers_behavior_boxselections', function (data, options) {


    //The drag box doesnt work if dragpan or navigation are enabled
    //turn off navigation on shift down
    $(document).keydown(function (e) {
      if(e.shiftKey) {
        var c = data.openlayers.getControlsBy('displayClass','olControlNavigation').pop() || {};
        if(c) {
          c.deactivate();
        }
      }
    }).keyup(function (e) {
      if(e.keyCode == 16) {//shift
        var c = data.openlayers.getControlsBy('displayClass','olControlNavigation').pop() || {};
        if(c) {
          c.activate();
        }
      }
    });

    var boxselection_layer = Drupal.behaviors.openlayers_behavior_boxselections.layer;

    //TODO add this to the settings of the bahaviour
    var featureLayer = data.openlayers.getLayersBy('drupalID', 'projects_projects_geo_list').pop();
    if(!boxselection_layer) {
      var boxselection_layer = new OpenLayers.Layer.Vector('Box Selection Layer');
        boxselection_layer.events.on({
        beforefeatureadded: function (event) {          
          poly = event.feature.geometry;

          var nid_filter = [];
          for (var a = 0; a < featureLayer.features.length; a++) {
            if (poly.intersects(featureLayer.features[a].geometry)) {
              var point = featureLayer.features[a];

              if(point.cluster) {
                $.each(point.cluster, function(k,v) {
                  var nid = this.data['nid'] || null;
                  if(nid) {
                    nid_filter.push(nid);
                  }
                });
              }
            }
          }

          //Somehowe configure this callback?
          var viewDomId = $('.view-projects').attr('class').match(/view-dom-id-(\S+)/).pop();
          var view = Drupal.settings.views.ajaxViews['views_dom_id:'+viewDomId] || {};
          if(view) {
            
            if(nid_filter.length) {
              $.unique(nid_filter);
              view.view_args = nid_filter.join(',');
              $('.view-projects .views-submit-button input').trigger('click');
              Drupal.behaviors.openlayers_behavior_boxselections.last_box = event.feature;
              Drupal.behaviors.openlayers_behavior_boxselections.layer = boxselection_layer;
            }
            else {
              view.view_args = '';
              return false;
            }
          }
        }
      });
      data.openlayers.addLayer(boxselection_layer);
    }
    else {
//      boxselection_layer.addFeatures([Drupal.behaviors.openlayers_behavior_boxselections.last_box]);
//      boxselection_layer.redraw();
    }

    var control = Drupal.behaviors.openlayers_behavior_boxselections.control;
    if(!control) {
      control = new OpenLayers.Control.DrawFeature(boxselection_layer, OpenLayers.Handler.RegularPolygon);
      control.handler.setOptions({
        'keyMask': OpenLayers.Handler.MOD_SHIFT,
        'sides': 4,
        'irregular': true
      });
      data.openlayers.addControl(control);
      control.activate();
    }
    else {
      control.activate();
      boxselection_layer.redraw();
    }

  });


})(jQuery);
