<?php
/**
 * @file
 * mpub_projectdb.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function mpub_projectdb_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'projects';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Projects';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'projects',
      'display' => 'projects_map_page',
      'args' => '',
      'title' => 'Map',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'projects',
      'display' => 'default',
      'args' => '',
      'title' => 'List',
      'weight' => '-99',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'ui_tabs';
  $quicktabs->style = 'Phylactere';
  $quicktabs->options = array(
    'history' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('List');
  t('Map');
  t('Projects');

  $export['projects'] = $quicktabs;

  return $export;
}
