<?php
/**
 * @file
 * mpub_projectdb.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function mpub_projectdb_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_access_control|node|project|form';
  $field_group->group_name = 'group_access_control';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'project';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Access Control',
    'weight' => '11',
    'children' => array(
      0 => 'field_access_control',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Access Control',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-access-control field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_access_control|node|project|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_agencies|node|project|form';
  $field_group->group_name = 'group_agencies';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'project';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Agencies',
    'weight' => '7',
    'children' => array(
      0 => 'field_associated_agencies',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Agencies',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-agencies field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_agencies|node|project|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_budget|node|project|form';
  $field_group->group_name = 'group_budget';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'project';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Budget',
    'weight' => '5',
    'children' => array(
      0 => 'field_project_budget',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Budget',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-budget field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_budget|node|project|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_documents_links|node|project|form';
  $field_group->group_name = 'group_documents_links';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'project';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Documents / links',
    'weight' => '8',
    'children' => array(
      0 => 'field_existing_documents_links',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Documents / links',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-documents-links field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_documents_links|node|project|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_goals|node|project|form';
  $field_group->group_name = 'group_goals';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'project';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Goals',
    'weight' => '12',
    'children' => array(
      0 => 'field_mdg_goal',
      1 => 'field_oceanscape_goal',
      2 => 'field_sdg_goal',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Goals',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-goals field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_goals|node|project|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_keyword|node|project|form';
  $field_group->group_name = 'group_keyword';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'project';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Keywords',
    'weight' => '9',
    'children' => array(
      0 => 'field_keywords',
      1 => 'field_species_targetted',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Keywords',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-keyword field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_keyword|node|project|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_location|node|project|form';
  $field_group->group_name = 'group_location';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'project';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Location',
    'weight' => '4',
    'children' => array(
      0 => 'field_locations',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Location',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-location field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_location|node|project|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_notes|node|project|form';
  $field_group->group_name = 'group_notes';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'project';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Notes',
    'weight' => '10',
    'children' => array(
      0 => 'field_notes',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Notes',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-notes field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_notes|node|project|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_point|field_collection_item|field_locations|form';
  $field_group->group_name = 'group_point';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_locations';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Add point manually',
    'weight' => '5',
    'children' => array(
      0 => 'field_location_geo',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Add point manually',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-point field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_point|field_collection_item|field_locations|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_project_dates|node|project|form';
  $field_group->group_name = 'group_project_dates';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'project';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Project Dates',
    'weight' => '3',
    'children' => array(
      0 => 'field_date',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Project Dates',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-project-dates field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_project_dates|node|project|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_researchers|node|project|form';
  $field_group->group_name = 'group_researchers';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'project';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Researchers',
    'weight' => '6',
    'children' => array(
      0 => 'field_officer',
      1 => 'field_researcher',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Researchers',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-researchers field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_researchers|node|project|form'] = $field_group;

  return $export;
}
