<?php
/**
 * @file
 * mpub_map_page.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function mpub_map_page_taxonomy_default_vocabularies() {
  return array(
    'map_categories' => array(
      'name' => 'Map Page Categories',
      'machine_name' => 'map_categories',
      'description' => 'Terms used to categorise map pages.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
