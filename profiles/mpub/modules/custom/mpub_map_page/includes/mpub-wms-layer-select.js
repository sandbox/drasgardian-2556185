(function($) {
  /**
   * Expand/collapse toggling for map layout.
   */
  Drupal.behaviors.mpub_layer_select = {
    
    update_meta: function($el) {      
      var layer = Drupal.settings.mpub_layers[$el.val()];

      if(typeof layer == 'undefined') {
        layer = {legend_url:'', metadata_url:''};
      }
      
      $el.parent().find('.legend-url').html(layer.legend_url);      
      $el.parent().find('.metadata-url').html(layer.metadata_url);
    },
    
    attach: function(context, settings) {      
      
      var parent = this;            
      
      $el = $(settings.mpub_wms_info_seleector);
      
      $el.change(function(){
        parent.update_meta($(this));        
      });      
      
      this.update_meta($el);        
    }
  };


})(jQuery);
