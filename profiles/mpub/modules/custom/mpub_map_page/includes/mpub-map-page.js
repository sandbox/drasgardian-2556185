(function($) {

  Drupal.behaviors.mpub_layer_zoom = {

    layer_extents: function(openlayers_data, layer_info) {
      var map = openlayers_data.map;
      var openLayersMap = $('#'+map.id).data('openlayers').openlayers;

      //console.log(map.layers[layer_info.machine_name].getDataExtent());

      var map_projection = new Proj4js.Proj(map.projection);
      var bounds = new OpenLayers.Bounds();

      var layer_projection = new Proj4js.Proj(layer_info.projection[0]);
      if(typeof layer_info.extent != 'undefined') {
        var extent = layer_info.extent;

        var maxXY = new Proj4js.Point(extent.westBoundLongitude, extent.northBoundLatitude);
        var minXY = new Proj4js.Point(extent.eastBoundLongitude, extent.southBoundLatitude);

        Proj4js.transform(layer_projection, map_projection, minXY);
        Proj4js.transform(layer_projection, map_projection, maxXY);

        bounds.extendXY(minXY.x, minXY.y);
        bounds.extendXY(maxXY.x, maxXY.y);

        //console.log(bounds);
      }
      else {
        $.each(openLayersMap.layers, function(index, openLayersLayer) {
          if(openLayersLayer.drupalID == layer_info.drupalID) {
            bounds = openLayersLayer.getDataExtent();
          }
        });
      }

      if(bounds) {
        openlayers_data.openlayers.zoomToExtent(bounds);
      }

    },
    go: function(layer_name) {
        var data = $('#openlayers-map').data('openlayers');
        var map_projection = new Proj4js.Proj(data.map.projection);
        var bounds = new OpenLayers.Bounds();
        var parent = this;

        $.each(Drupal.settings.mpub_layers, function(k, v){
          if(typeof layer_name != 'undefined') {
            if(layer_name != k) {
              return true;//continue;
            }
          }
          else if(!$('.layer[data-layer-name="'+k+'"]:checked').length) {
            return true;//continue
          }

          this.drupalID = k;
          parent.layer_extents(data, this);

        });
      },
  };


  /**
   * Expand/collapse toggling for map layout.
   */
  Drupal.behaviors.map_page_format_download = {
    attach: function(context, settings) {
      //trigger download of map format
      $('select#map-format-select').change(function(){
        $('a#map-format-download-link').attr('href',$(this).val());
        $('a#map-format-download-link').text('Download '+$(this).find(':selected').text());
      });
    }
  };

  /**
   * Expand/collapse toggling for map layout.
   */
  Drupal.behaviors.map_page_toggle = {
    refreshMap: function() {

    },
    attach: function(context, settings) {

      $('a.toggle-info').once('map_page_toggle').click(function(event){
        if( !$('.map-layout').hasClass('open-right')) {
          $('.map-layout td.toggle-right').trigger('click');
        }
      });

      $('.map-layout .toggle-left').once('map_page_toggle').click(function() {
        var toggle = $(this);

        if($('.map-layers-sidebar').css('display') == 'none') {
          toggle.parents('.map-layout').removeClass('collapsed-left');
          toggle.parents('.map-layout').addClass('open-left');
        }
        else {
          toggle.parents('.map-layout').addClass('collapsed-left');
          toggle.parents('.map-layout').removeClass('open-left');
        }


        toggle.find('.arrow').toggleClass('arrow-left').toggleClass('arrow-right');
        refreshMap();
        updateMap();
      });

      $('.map-layout .toggle-right').once('map_page_toggle').click(function() {
        var toggle = $(this);
        toggle.parents('.map-layout').toggleClass('collapsed-right');
        toggle.parents('.map-layout').toggleClass('open-right');
        toggle.find('.arrow').toggleClass('arrow-left').toggleClass('arrow-right');
        refreshMap();
        updateMap();
      });
    }
  };


  /**
   * Updates the map when container size has changed.
   */
  function refreshMap() {
    for (id in Drupal.settings.openlayers.maps) {
      var map = $('#' + id);
      if (map.length > 0) {
        map.data('openlayers').openlayers.updateSize();
      }
    }
  };

  Drupal.behaviors.map_page_refreshMap = function() {
    for (id in Drupal.settings.openlayers.maps) {
      var map = $('#' + id);
      if (map.length > 0) {
        map.data('openlayers').openlayers.updateSize();
      }
    }
  }


  /**
    Make the map fill the screen
  **/

  var updateMap = function() {
    // Map
    var map = $('#openlayers-map').data('openlayers');
    var height = $(window).height() - ($('.l-header').outerHeight(true) + parseInt($('body').css('margin-top')));

    if(height < 400) {
      return;
    }

    var width = $('#openlayers-map').width();
    $('.alert').each(function(index) {
      height -= $(this).outerHeight(true);
    });


    $('#map-map .openlayers-views-map').css({'height': height+'px'});
    $('#openlayers-container-openlayers-map').css({'height': height+'px'});
    $('#openlayers-map').css({'height': height+'px'});

    $('.map-layers-sidebar .map-sidebar-content').css({'height': (height - $('.map-layers-sidebar h2.map-sidebar-heading').outerHeight(true) )+'px'});
    $('.map-info-sidebar .map-sidebar-content').css({'height': (height - $('.map-info-sidebar h2.map-sidebar-heading').outerHeight(true) )+'px'});

    if(map) {
      map.openlayers.updateSize();
    }

    $('#openlayers-map').once('initial-zoom', function(){
      Drupal.behaviors.mpub_layer_zoom.go();
    });

  }


  $(document).bind('ready ajaxComplete', updateMap);
  $(window).resize(updateMap);


})(jQuery);
