<?php

module_load_include('php', 'openlayers_ui', 'plugins/export_ui/openlayers_layers_ui.class');
/**
 * @file
 */

class mpub_layer_layers_ui extends openlayers_layers_ui {

  /**
   * hook_menu() entry point.
   *
   * Child implementations that need to add or modify menu items should
   * probably call parent::hook_menu($items) and then modify as needed.
   */
  function hook_menu(&$items) {
    parent::hook_menu($items);
    $items['admin/structure/openlayers/mpublayers']['type'] = MENU_LOCAL_TASK;
  }


  /**
   * Build a row based on the item.
   *
   * By default all of the rows are placed into a table by the render
   * method, so this is building up a row suitable for theme('table').
   * This doesn't have to be true if you override both.
   */
  function list_build_row($item, &$form_state, $operations) {
    // Set up sorting
    $name = $item->{$this->plugin['export']['key']};
    $schema = ctools_export_get_schema($this->plugin['schema']);

    $layers_types = openlayers_layer_types();
    if (!isset($layers_types[$item->data['layer_type']])) {
      return;
    }

    // Note: $item->{$schema['export']['export type string']} should have already been set up by export.inc so
    // we can use it safely.
    switch ($form_state['values']['order']) {
      case 'disabled':
        $this->sorts[$name] = empty($item->disabled) . $name;
        break;
      case 'title':
        $this->sorts[$name] = $item->{$this->plugin['export']['admin_title']};
        break;
      case 'name':
        $this->sorts[$name] = $name;
        break;
      case 'storage':
        $this->sorts[$name] = $item->{$schema['export']['export type string']} . $name;
        break;
    }

    $this->rows[$name]['data'] = array();
    $this->rows[$name]['class'] = !empty($item->disabled) ? array('ctools-export-ui-disabled') : array('ctools-export-ui-enabled');

    // If we have an admin title, make it the first row.
    if (!empty($this->plugin['export']['admin_title'])) {
      $this->rows[$name]['data'][] = array('data' => check_plain($item->{$this->plugin['export']['admin_title']}), 'class' => array('ctools-export-ui-title'));
    }

    $this->rows[$name]['data'][] = array('data' => $item->title, 'class' => array('ctools-export-ui-title'));
    $this->rows[$name]['data'][] = array('data' => $layers_types[$item->data['layer_type']]['title'], 'class' => array('ctools-export-ui-layer-type'));
    $this->rows[$name]['data'][] = array('data' => $item->description, 'class' => array('ctools-export-ui-description'));
    $this->rows[$name]['data'][] = array('data' => check_plain($item->{$schema['export']['export type string']}), 'class' => array('ctools-export-ui-storage'));

    // This should be in the module openlayers_views, but I'm still looking
    // for a solution to do it properly.
    // Temporarily removed.
    /*
    if ($item->data['layer_type'] == 'openlayers_views_vector') {
      $operations['edit']['href'] = 'admin/structure/views/view/' . $item->data['views']['view'] . '/edit/' . $item->data['views']['display'];
      $operations['disable']['href'] = 'admin/structure/views/view/' . $item->data['views']['view'] . '/disable/' . $item->data['views']['display'];
      $operations['clone']['href'] = 'admin/structure/views/view/' . $item->data['views']['view'] . '/clone/' . $item->data['views']['display'];
      $operations['export']['href'] = 'admin/structure/views/view/' . $item->data['views']['view'] . '/export/' . $item->data['views']['display'];
    }
    */
    $ops = theme('links__ctools_dropbutton', array('links' => $operations, 'attributes' => array('class' => array('links', 'inline'))));

    $this->rows[$name]['data'][] = array('data' => $ops, 'class' => array('ctools-export-ui-operations'));

    // Add an automatic mouseover of the description if one exists.
    if (!empty($this->plugin['export']['admin_description'])) {
      $this->rows[$name]['title'] = $item->{$this->plugin['export']['admin_description']};
    }

    if (isset($item->data['params']['layer_group'])) {
      $this->rows[$name]['group'] = $item->data['params']['layer_group'];
    }
  }

  /**
   * Provide the table header.
   *
   * If you've added columns via list_build_row() but are still using a
   * table, override this method to set up the table header.
   */
  function list_table_header() {
    $header = array();


    if (!empty($this->plugin['export']['admin_title'])) {
      $header[] = array('data' => t('Name'), 'class' => array('ctools-export-ui-name'));
    }

    $header[] = array('data' => t('Title'), 'class' => array('ctools-export-ui-title'));
    $header[] = array('data' => t('Type'), 'class' => array('ctools-export-ui-layer-type'));
    $header[] = array('data' => t('Description'), 'class' => array('ctools-export-ui-description'));
    $header[] = array('data' => t('Storage'), 'class' => array('ctools-export-ui-storage'));
    $header[] = array('data' => t('Operations'), 'class' => array('ctools-export-ui-operations'));

    return $header;
  }

  /**
   * Render all of the rows together.
   *
   * By default we place all of the rows in a table, and this should be the
   * way most lists will go.
   *
   * Whatever you do if this method is overridden, the ID is important for AJAX
   * so be sure it exists.
   */
  function list_render(&$form_state) {

    $container = array(
      '#type' => 'container',
      '#attributes' => array(
        'id' => array('ctools-export-ui-list-items'),
      ),
    );


    $grouped_rows = array();

    foreach ($this->rows as $row) {
      if (isset($row['group'])) {
        $grouped_rows[$row['group']][] = $row;
      }
      else {
        $grouped_rows['other'][] = $row;
      }
    }

    // sort by the grouping field
    asort($grouped_rows);
    //move other to the end
    if (isset($grouped_rows['other'])) {
      $other = $grouped_rows['other'];
      unset($grouped_rows['other']);
      $grouped_rows['other'] = $other;
    }

    foreach ($grouped_rows as $group_name => $group) {
      $container[$group_name] = array(
        '#header' => $this->list_table_header(),
        '#rows' => $group,
        '#empty' => $this->plugin['strings']['message']['no items'],
        '#prefix' => '<h3>' . $group_name . '</h3>',
        '#theme' => 'table',
      );
    }

    return drupal_render($container);
  }

  // /**
  //  * Provide a list of sort options.
  //  *
  //  * Override this if you wish to provide more or change how these work.
  //  * The actual handling of the sorting will happen in build_row().
  //  */
  // function list_sort_options() {
  //   if (!empty($this->plugin['export']['admin_title'])) {
  //     $options = array(
  //       'disabled' => t('Enabled, title'),
  //       $this->plugin['export']['admin_title'] => t('Title'),
  //     );
  //   }
  //   else {
  //     $options = array(
  //       'disabled' => t('Enabled, title'),
  //     );
  //   }

  //   $options += array(
  //     'storage' => t('Storage'),
  //   );

  //   return $options;
  // }

}
