<?php


/**
 * Implements hook_ctools_plugin_directory().
 */
function mpub_layer_ctools_plugin_directory($module, $plugin) {
  if ($module == 'ctools' && $plugin == 'export_ui') {
    return 'plugins/' . $plugin;
  }
}


function mpub_layer_ctools_plugin_api($module, $api) {
  if ($module == "openlayers") {
    switch ($api) {
      case 'openlayers_layers':
        return array('version' => 1);
    }
  }
}


