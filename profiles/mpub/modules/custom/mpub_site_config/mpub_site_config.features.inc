<?php
/**
 * @file
 * mpub_site_config.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mpub_site_config_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == 'panels_mini' && $api == 'panels_default') {
    return array('version' => 1);
  }

  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }

}
