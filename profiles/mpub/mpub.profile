<?php

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function mpub_form_install_configure_form_alter(&$form, $form_state) {
}
