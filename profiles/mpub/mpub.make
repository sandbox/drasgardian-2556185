api = 2
core = 7.x
defaults[projects][subdir] = contrib

; Modules
projects[admin_menu][version] = "3.0-rc5"
projects[autocomplete_deluxe][version] = "2.1"
projects[better_exposed_filters][version] = "3.2"
projects[ctools][version] = "1.7"
projects[date][version] = "2.8"
projects[diff][version] = "3.2"
projects[ds][version] = "2.10"
projects[email][version] = "1.3"
projects[entity][version] = "1.6"
projects[features][version] = "2.3"
projects[field_collection][version] = "1.0-beta8"
projects[fontawesome][version] = "2.1"
projects[geofield][version] = "2.3"
projects[geofield][patch][] = "https://www.drupal.org/files/issues/openlayers-widget-show-title-description-2045325-5.patch"
projects[geofield][patch][] = "https://www.drupal.org/files/issues/multiple-draw-widgets-patch-2041169-6.patch"
projects[geophp][version] = "1.7"
projects[panels][version] = "3.5"
projects[imce][version] = "1.9"
projects[imce_wysiwyg][version] = "1.0"
projects[kml][version] = "1.0-alpha1"
projects[libraries][version] = "2.2"
projects[link][version] = "1.3"
projects[menu_admin_per_menu][version] = "1.0"
projects[nice_menus][version] = "2.5"
projects[openlayers][version] = "2.0-beta11"
projects[pathauto][version] = "1.2"
projects[proj4js][version] = "1.2"
projects[quicktabs][version] = "3.6"
projects[responsive_menus][version] = "1.5"
projects[strongarm][version] = "2.0"
projects[taxonomy_access_fix][version] = "2.1"
projects[token][version] = "1.5"
projects[views][version] = "3.11"
projects[views_data_export][version] = "3.0-beta8"
projects[views_responsive_grid][version] = "1.3"
projects[wysiwyg][version] = "2.2+54-dev"
projects[wysiwyg_filter][version] = "1.6-rc2"
projects[bean][version] = "1.9"

; Themes
projects[omega][version] = "4.3"

;Dev version of wysiwyg for CKEditor 4.x
projects[wysiwyg][version] = "2.x-dev"
projects[wysiwyg][download][type] = "git"
projects[wysiwyg][download][url] = "http://git.drupal.org/project/wysiwyg.git"
projects[wysiwyg][download][revision] = "898d022cf7d0b6c6a6e7d813199d561b4ad39f8b"

; Dev version of SHS because it hasn't had a release since 2013
projects[shs][version] = "1.x-dev"
projects[shs][download][type] = "git"
projects[shs][download][url] = "http://git.drupal.org/project/shs.git"
projects[shs][download][revision] = "8ee12039fe5324dc98c171a065bfeb4c2ab68a54"

; Dev version of hierarchical_term_formatter because of
; https://www.drupal.org/node/2469433
projects[hierarchical_term_formatter][version] = "1.x-dev"
projects[hierarchical_term_formatter][download][type] = "git"
projects[hierarchical_term_formatter][download][url] = "http://git.drupal.org/project/hierarchical_term_formatter.git"
projects[hierarchical_term_formatter][download][revision] = "e5dac7ff0dcca0c90797930584d673fb6bb711e2"
