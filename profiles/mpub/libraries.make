api = 2
core = 7.x

; Use the following command to download the required libraries:
; drush make --no-core libraries.make --contrib-destination=.

;Libraries
libraries[openlayers][download][type] = "file"
libraries[openlayers][download][url] = http://github.com/openlayers/openlayers/releases/download/release-2.13.1/OpenLayers-2.13.1.zip
libraries[openlayers][directory_name] = openlayers

libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.1/ckeditor_4.4.1_standard.zip
libraries[ckeditor][directory_name] = ckeditor

libraries[proj4js][download][type] = "git"
libraries[proj4js][download][url] = "https://github.com/proj4js/proj4js.git"
libraries[proj4js][download][revision] = "21596cb8b370b61cc6a6dfdd99fcbebbacf17004"
libraries[proj4js][directory_name] = "proj4js"
libraries[proj4js][type] = "library"

libraries[fontawesome][download][type] = "file"
libraries[fontawesome][download][url] = "https://fortawesome.github.io/Font-Awesome/assets/font-awesome-4.4.0.zip"
libraries[fontawesome][directory_name] = "fontawesome"
libraries[fontawesome][type] = "library"
