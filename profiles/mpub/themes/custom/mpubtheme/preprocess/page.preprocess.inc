<?php

/**
 * Implements hook_preprocess_page().
 */
function mpubtheme_preprocess_page(&$variables) {

  $variables['footer_menu'] = menu_navigation_links('menu-footer-menu');

  if (module_exists('nice_menus')) {
    $nice_menu = theme('nice_menus', array(
      'id' => 0,
      'menu_name' => variable_get('menu_main_links_source', 'main-menu'),
      'mlid' => 0,
      'direction' => 'down',
      'depth' => 3,
      ));
    $variables['page']['navigation']['main_menu']['#markup'] = $nice_menu['content'];
  }
  else {
    $variables['page']['navigation']['main_menu'] = array(
      '#theme' => 'links__system_main_menu',
      '#links' => $variables['main_menu'],
      '#attributes' => array('class' => array('main-menu', 'links', 'inline', 'clearfix')),
    );
  }

  $variables['page']['footer_menu']['footer_menu'] = array(
    '#theme' => 'links__system_footer_menu',
    '#links' => $variables['footer_menu'],
    '#attributes' => array('class' => array('footer-menu', 'links', 'inline', 'clearfix')),
  );

  $tabs = &$variables['tabs'];
  $variables['icon_tabs'] = array();
  if( !empty($tabs['#primary']) ) {

    $fa_icons = array(
      'Edit' => '<i class="fa fa-edit"></i>',
      'View' => '<i class="fa fa-eye"></i>',
    );

    foreach($tabs['#primary'] as $i => &$task) {
      if(isset($fa_icons[$task['#link']['title']])) {
        $title = $fa_icons[$task['#link']['title']].'&nbsp;'.$task['#link']['title'];
        $variables['icon_tabs'][] = array(
          '#markup' => l($title, $task['#link']['href'], array('html' => 1))
        );
      }
    }
  }

  // breadcrumb handling
  if ($node = menu_get_object()) {
    switch ($node->type) {
      case 'map_page':
        $bc = array(
          l(t('Home'),'<front>'),
          l(t('Maps'), 'maps'),
          views_trim_text(array('max_length' => 50, 'word_boundary' => TRUE, 'ellipsis' => TRUE),drupal_get_title()),
        );
        drupal_set_breadcrumb($bc);

        // add fa icon to page title
        drupal_set_title('<i class="fa fa-map-o"></i>' . drupal_get_title(), PASS_THROUGH);
      break;

      case 'project':
        $bc = array(
          l(t('Home'),'<front>'),
          l(t('Projects'), 'projects'),
          views_trim_text(array('max_length' => 50, 'word_boundary' => TRUE, 'ellipsis' => TRUE),drupal_get_title()),
        );

        drupal_set_breadcrumb($bc);

        // add fa icon to page title
        drupal_set_title('<i class="fa fa-paperclip"></i>' . drupal_get_title(), PASS_THROUGH);
      break;
      default:
        $bc = drupal_get_breadcrumb();
        $bc[] = drupal_get_title();
        drupal_set_breadcrumb($bc);
      break;
    }
  }
  else if (drupal_is_front_page()) {
    // no breadcrumbs on homepage
    drupal_set_breadcrumb(array());
  }
  else {
    $bc = drupal_get_breadcrumb();
    $bc[] = drupal_get_title();
    drupal_set_breadcrumb($bc);
  }


  switch (current_path()) {
    case 'maps':
      drupal_set_title('<i class="fa fa-map-o"></i>' . drupal_get_title(), PASS_THROUGH);
    break;
    case 'projects':
      drupal_set_title('<i class="fa fa-paperclip"></i>' . drupal_get_title(), PASS_THROUGH);
    break;
  }
}
