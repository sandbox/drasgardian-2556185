<div<?php print $attributes; ?>>
  <header class="l-header" role="banner">
    <div class="container clearfix">
      <div class="branding">
        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
        <?php endif; ?>

        <?php if ($site_name || $site_slogan): ?>
          <div class="site-particulars">
            <?php if ($site_name): ?>
              <h1 class="site-name">
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </h1>
            <?php endif; ?>

            <?php if ($site_slogan): ?>
              <h2 class="site-slogan"><?php print $site_slogan; ?></h2>
            <?php endif; ?>
          </div>
        <?php endif; ?>
      </div>

      <div class="navigation">
        <?php print render($page['navigation']); ?>
      </div>

      <?php print render($page['header']); ?>
    </div>
  </header>

  <a id="main-content"></a>

  <main class="l-main">
    <?php if($breadcrumb) : ?>
      <div class="breadcrumb-row">
        <div class="container clearfix">
          <?php print $breadcrumb; ?>
        </div>
      </div>
    <?php endif; ?>
    <div class="l-subheader">
      <?php if ($title && !$is_front): ?>
        <header class="title-bar container">
          <h1 class="title"><?php print $title; ?>
            <span class="mpub-print-link"></span>
          </h1>
        </header>
      <?php endif; ?>
      <?php print render($page['messages']); ?>
      <?php print $messages; ?>
      <?php if ($icon_tabs): ?>
        <div id="tabs" class="container">
          <?php foreach($icon_tabs as $tab): ?>
            <?php print render($tab) ?>
          <?php endforeach; ?>
        </div>
      <?php endif; ?>
    </div>

    <div class="l-content container clearfix" role="main">
      <?php print render($page['content']); ?>
    </div>
  </main>

  <div class="l-fullheight">

  <footer class="l-footer" role="contentinfo">
    <div class="container clearfix">
      <div class="r-footer-content">
        <?php print render($page['footer']); ?>
      </div>
      <div class="r-footer-credits">
        <div class="copyright-message"><?php print variable_get('copyright_message', '&copy; All rights reservered 2015'); ?></div>
        <?php if ($page['footer_menu']): ?>
          <?php print render($page['footer_menu']); ?>
        <?php endif; ?>
      </div>
    </div>
  </footer>
</div>
