<?php

/**
 * @file
 * Defines an asymetric, golden ratio based panels layout.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Responsive 2 column'),
  'icon' => 'preview.png',
  'category' => t('Omega content layouts'),
  'theme' => 'resp_2col',
  'css' => '../../../css/layouts/resp-2col/resp-2col.layout.css',
  'regions' => array(
    'first' => t('First'),
    'second' => t('Second'),
  ),
);

/**
 * Implements hook_preprocess_golden().
 */
function template_preprocess_resp_2col(&$variables) {
  $variables['attributes_array']['class'][] = 'panel-resp-2col';
  $variables['attributes_array']['class'][] = 'panel-display--resp-2col';

  foreach($variables['content'] as $name => $item) {
    $variables['region_attributes_array'][$name]['class'][] = 'resp-2col-region';
    $variables['region_attributes_array'][$name]['class'][] = 'resp-2col-region--' . drupal_clean_css_identifier($name);
  }
}

