<?php

/**
 * @file
 * Defines an asymetric, golden ratio based panels layout.
 */

// Plugin definition.
$plugin = array(
  'title' => t('MPub Homepage Layout'),
  'icon' => 'preview.png',
  'category' => t('Omega content layouts'),
  'theme' => 'mpub_home',
  'css' => '../../../css/layouts/mpub-home/mpub-home.layout.css',
  'regions' => array(
    'top' => t('Top'),
    'featured_content' => t('Featured Content'),
    'maps_overview' => t('Maps Overview'),
    'projects_overview' => t('Projects Overview'),
    'documents_overview' => t('Documents Overview'),
    'bottom' => t('Bottom'),
  ),
);

/**
 * Implements hook_preprocess_golden().
 */
function template_preprocess_mpub_home(&$variables) {
  drupal_add_library ( 'system' , 'ui.tabs' );

  $variables['attributes_array']['class'][] = 'panel-mpub-home';
  $variables['attributes_array']['class'][] = 'panel-display--mpub-home';

  foreach($variables['content'] as $name => $item) {
    $variables['region_attributes_array'][$name]['class'][] = 'mpub-home-region';
    $variables['region_attributes_array'][$name]['class'][] = 'mpub-home-region--' . drupal_clean_css_identifier($name);

    $variables['region_attributes_array'][$name]['id'] = str_replace('_', '-', $name);
  }

}

