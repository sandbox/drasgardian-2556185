<?php

/**
 * @file
 * Banner image bean layout
 */
?>

<div class="banner-image">
	<img src="<?php print file_create_url($field_banner_image[0]['uri']); ?>" alt="<?php print $title; ?>" />
  <div class="banner-content">
    <h2><?php print $title; ?></h2>
    <?php $field = field_view_field('bean', $variables['elements']['#entity'], 'field_body', 'default'); print drupal_render($field); ?>
    <?php $field = field_view_field('bean', $variables['elements']['#entity'], 'field_link', 'default'); print drupal_render($field); ?>
  </div>
</div>

